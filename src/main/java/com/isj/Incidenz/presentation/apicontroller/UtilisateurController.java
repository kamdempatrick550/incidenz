package com.isj.Incidenz.presentation.apicontroller;

import com.isj.Incidenz.model.Utilisateur;
import com.isj.Incidenz.service.IUtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/utilisateur/")
@CrossOrigin
public class UtilisateurController {
    @Autowired
    private IUtilisateurService iUtilisateurService;

    @PostMapping("enregistrer")
    public Utilisateur enregistrer_user(@RequestBody Utilisateur utilisateur){
        return iUtilisateurService.enregistrerUtilisateur(utilisateur);
    }

    @GetMapping("liste")
    private List<Utilisateur> listeUser(){
        return iUtilisateurService.listeUtilisateur();
    }
}
