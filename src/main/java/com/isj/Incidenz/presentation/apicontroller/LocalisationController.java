package com.isj.Incidenz.presentation.apicontroller;

import com.isj.Incidenz.model.Localisation;
import com.isj.Incidenz.service.LocalisationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("api/localisation/")
public class LocalisationController {
    @Autowired
    private LocalisationService localisationService;

    @PostMapping("enregistrer")
    public Localisation enregistrer_user(@RequestBody Localisation localisation){
        localisation.setPays("Cameroun");
        return localisationService.enregistrerLocalisation(localisation);
    }
     @GetMapping("liste")
    public List<Localisation> localisations(){
        return localisationService.listeLocalisation();
     }

     @GetMapping("distance")
     public double distance(double lon1, double lat1,double lon2,double lat2) {//Recupere les latitudes et longitudes et renvoit la distance
         lon1=Math.toRadians(lon1);
         lon2=Math.toRadians(lon2);
         lat1=Math.toRadians(lat1);
         lat2=Math.toRadians(lat2);

         double d= 6371 * Math.acos( Math.sin(lat1)*Math.sin(lat2) +  Math.cos(lat1)*Math.cos(lat2)*Math.cos(lon2-lon1) );
         System.out.println(d);
         return d;
     }
}
