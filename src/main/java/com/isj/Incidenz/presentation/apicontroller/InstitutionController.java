package com.isj.Incidenz.presentation.apicontroller;


import com.isj.Incidenz.model.*;
import com.isj.Incidenz.service.IncidentService;
import com.isj.Incidenz.service.InstitutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("api/institution/")
@CrossOrigin
public class InstitutionController {

    @Autowired
    private InstitutionService institutionService;
    @Autowired
    private IncidentService incidentService;

    @PostMapping("enregistrer")
    public Institution enregistrer_institution(@RequestBody Institution institution){
        institution.setRole(Role.institution);
        return institutionService.enregistrerInstitution(institution);
    }

    @PostMapping("enregistrerform")
    public Institution enregistrer_institution_form(@RequestBody Institution institution){

        return institutionService.enregistrerInstitution(institution);
    }

    @GetMapping("RechTypeInstitution/{type}")
    public Set<Institution> type(@PathVariable TypeInstituton type){
        return institutionService.rechercherParType(type);
    }

    @GetMapping("liste")
    public List<Institution> institutionList(){
        return institutionService.listeInstitution();
    }

    @GetMapping("typeInstitution")
    public TypeInstituton[] typeInstitutons(){
        return TypeInstituton.values();
    }

    @GetMapping("connexion/{email}/{mdp}")
    public Institution connexion(@PathVariable String email, @PathVariable String mdp){
        return institutionService.rechercherEmailMpd(email,mdp);
    }

    @PutMapping("modifier")
    public Institution modifierInstitution(@RequestBody Institution institution){
        return institutionService.enregistrerInstitution(institution);
    }

    //@GetMapping("contains/{nom}/{id}")
    public Boolean testContains(@PathVariable String nom,@PathVariable int id){
        Institution institution=institutionService.rechercherParNom(nom);
        Incident incident=incidentService.rechercherIncident(id);
        List<Incident> incidents=incidentService.listeIncident();

            for (Institution inc:
                 incident.getInstitutionsConcerne()) {
                if (inc.getIdInstitution()==institution.getIdInstitution()){
                    return true;
                }
            }


        return false;
    }

    @GetMapping("tauxResolution/{nom}")
    public float tauxDeResolution(@PathVariable String nom){
        Institution institution=institutionService.rechercherParNom(nom);
        List<Incident> incidents=incidentService.listeIncident();
        float taux=0;
        float nbreActu=0;
        float nbreTotal=0;
        boolean contient;


        for (Incident i:
             incidents) {
            contient=false;
            nbreTotal++;
            if (i.getStatutIncident().toString().equals(Statut.Resolu.toString())){
                contient=testContains(nom,i.getIdIncident());
                if (contient){
                    nbreActu++;
                }
            }
        }
        System.out.println(nbreActu);
        System.out.println(nbreTotal);
        taux=(nbreActu/nbreTotal)*100;
        taux= (float) (Math.round(taux*100.0)/100.0);
        return taux;
    }
}
