package com.isj.Incidenz.presentation.apicontroller;


import com.isj.Incidenz.model.*;
import com.isj.Incidenz.service.IncidentService;
import com.isj.Incidenz.service.InstitutionService;
import com.isj.Incidenz.service.LocalisationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("api/incident/")
@CrossOrigin
public class IncidentController {

    @Autowired
    private IncidentService incidentService;

    @Autowired
    private LocalisationService localisationService;

    @Autowired
    private InstitutionService institutionService;

    @PostMapping("testSuggContains/{id}")
    public Boolean testSuggContains(@RequestBody Set<Incident> incidentSet,@PathVariable int id){

        for (Incident i:
             incidentSet) {
            if (i.getIdIncident()==id){
                return true;
            }
        }
        return false;
    }

    //Suggerer a l'institution les incidents qui sont similaire a un incident choisi
    @GetMapping("suggerer/{id}")
    public Set<Incident> suggerericident(@PathVariable int id){
        Incident incident=incidentService.rechercherIncident(id);
        List<Incident> incidents= incidentService.listeIncident();
        Set<Incident> incidentSet= new HashSet<>();
        boolean position;
        double distance;
        int cmp;

        for(Incident i:incidents){
            //boolean[] comparaison= new boolean[]{false,false,false,false,false};
            cmp=0;
            distance=-1;
            position=false;

            if (i.getIdIncident()==incident.getIdIncident()  || testSuggContains(incident.getIncidentsSimilaire(),i.getIdIncident()) ){
                position=true;
            }
            else {
                double lon1=Double.parseDouble(i.getLocalisation().getLongitude());
                double lat1=Double.parseDouble(i.getLocalisation().getLatitude());
                double lon2=Double.parseDouble(incident.getLocalisation().getLongitude());
                double lat2=Double.parseDouble(incident.getLocalisation().getLatitude());
                //distance entre les deux points
                distance=localisationService.distance(lon1,lat1,lon2,lat2);
                if (lon1!=181 && lon2!=181 && lat1!=86 && lat2!=86){
                    position=true;
                }

                if (incident.getLocalisation().getQuartier().equals(i.getLocalisation().getQuartier())){
//                comparaison[0]=true;
                    cmp++;
                }
                if (incident.getLocalisation().getVille().equals(i.getLocalisation().getVille())){
//                comparaison[1]=true;
                    cmp++;
                }
                if (incident.getLocalisation().getPays().equals(i.getLocalisation().getPays())){
//                comparaison[2]=true;
                    cmp++;
                }
                if (Math.abs(incident.getDate().getTime()-i.getDate().getTime())<=3600000){
                    System.out.println(incident.getDate().getTime()-i.getDate().getTime());
                    System.out.println(incident.getDate().getTime());
                    System.out.println(incident.getDate().getTime());
//                comparaison[3]=true;
                    cmp++;
                }
                if (incident.getTypeIncident().equals(i.getTypeIncident())){
//                comparaison[4]=true;
                    cmp++;
                }
                if (distance<=0.2 && distance>=0 && position){//distance<= 200m
                    cmp++;
                }

                System.out.println("valeur finale:"+cmp);
                if (cmp>=5){
                    incidentSet.add(i);
                }
            }

        }

        return incidentSet;
    }

    @PostMapping("enregistrer")
    public Incident enregistrerIncident(@RequestBody Incident incident){
        incident.setDate(new Date());
        incident.setStatutIncident(Statut.signalée);
        Set<Institution> police=institutionService.rechercherParType(TypeInstituton.police);
        Set<Institution> pompier=institutionService.rechercherParType(TypeInstituton.pompier);
        Set<Institution> hopital=institutionService.rechercherParType(TypeInstituton.hopital);
        Set<Institution> electrique=institutionService.rechercherParType(TypeInstituton.electrique);



        if (incident.getTypeIncident().toString().equals(TypeIncident.incendie.toString())){
            incident.getInstitutionsConcerne().addAll(pompier);
            incident.getInstitutionsConcerne().addAll(hopital);
        } else
        if (incident.getTypeIncident().toString().equals(TypeIncident.accident.toString())){
            incident.getInstitutionsConcerne().addAll(hopital);
        } else
        if (incident.getTypeIncident().toString().equals(TypeIncident.Incident_électrique.toString())){
            incident.getInstitutionsConcerne().addAll(hopital);
            incident.getInstitutionsConcerne().addAll(electrique);
        } else
            if (incident.getTypeIncident().toString().equals(TypeIncident.meurtre.toString())){
            incident.getInstitutionsConcerne().addAll(hopital);
            incident.getInstitutionsConcerne().addAll(police);
        } else
            if (incident.getTypeIncident().toString().equals(TypeIncident.vol.toString())){
                incident.getInstitutionsConcerne().addAll(police);
            }

            return incidentService.enregistrerIncident(incident);
    }


    @GetMapping("liste")
    public List<Incident> incidentList(){
        return incidentService.listeIncident();
    }

    @GetMapping("typeIncident")
    public TypeIncident[] typeIncidents(){
        return TypeIncident.values();
    }

    @GetMapping("listeRecent")
    public Page<Incident> pageRecentes( Pageable pageable){
        return incidentService.incdentRecents(pageable);
    }

    @PutMapping("modifier")
    public Incident modifierIncident(@RequestBody Incident incident){
        return incidentService.enregistrerIncident(incident);
    }

    @GetMapping("rechercher/{id}")
    public Incident rechercher(@PathVariable int id){
        return incidentService.rechercherIncident(id);
    }

    //Test du contains...NON CONCLUANT
    @GetMapping("simil/{id}")
    public Set<Incident> simil(@PathVariable int id){
        return incidentService.rechercherIncident(id).getIncidentsSimilaire();
    }

    //enregistrer une institution comme etant similaire a une autre
    @PutMapping("ajoutsuggestion/{idSrc}")
    public Incident ajouterSuggestion(@PathVariable int idSrc,@RequestBody Incident incidentSugg){
        Incident incidentSrc=incidentService.rechercherIncident(idSrc);
        Incident incident2=incidentService.rechercherIncident(incidentSugg.getIdIncident());
        //Incident incidentSugg=incidentService.rechercherIncident(idSugg);

        //A Verifier
        incident2.setStatutIncident(incidentSrc.getStatutIncident());
        incident2.getIncidentsSimilaire().add(incidentSrc);

        incidentSrc.getIncidentsSimilaire().add(incident2);
        incidentService.enregistrerIncident(incident2);
        return incidentService.enregistrerIncident(incidentSrc);
    }

    //Faire la correspondance pour que chaque institution voit les incidents la concernant
    @GetMapping("inCidInstit/{statutIncident}/{typeInstitution}")
    public Set<Incident> correspondance(@PathVariable String statutIncident,@PathVariable String typeInstitution){
        List<Incident> incidents = incidentService.listeIncident();
        Set<Incident> incidentSet=new HashSet<>();

        ArrayList<String> police=new ArrayList<>();
        police.add("vol");
        police.add("meurtre");

        ArrayList<String> pompier=new ArrayList<>();
        pompier.add("incendie");

        ArrayList<String> hopital=new ArrayList<>();
        hopital.add("accident");
        hopital.add("incendie");
        hopital.add("meurtre");
        hopital.add("Incident_électrique");

        ArrayList<String> electrique=new ArrayList<>();
        electrique.add("Incident_électrique");

        if (typeInstitution.equals("police")){
            for (Incident i:
                    incidents) {
                if (statutIncident.equals(i.getStatutIncident().toString()) && police.contains(i.getTypeIncident().toString()) ){
                    incidentSet.add(i);
                }
            }
        }

        if (typeInstitution.equals("hopital")){
            for (Incident i:
                    incidents) {
                if (statutIncident.equals(i.getStatutIncident().toString()) && hopital.contains(i.getTypeIncident().toString()) ){
                    incidentSet.add(i);
                }
            }
        }

        if (typeInstitution.equals("electrique")){
            for (Incident i:
                    incidents) {
                if (statutIncident.equals(i.getStatutIncident().toString()) && electrique.contains(i.getTypeIncident().toString()) ){
                    incidentSet.add(i);
                }
            }
        }
        if (typeInstitution.equals("pompier")){
            for (Incident i:
                    incidents) {
                if (statutIncident.equals(i.getStatutIncident().toString()) && pompier.contains(i.getTypeIncident().toString()) ){
                    incidentSet.add(i);
                }
            }
        }
        return incidentSet;
    }

    //Mettre a jour le statut de l'incident et des incidents enregistre comme similaire par l'institution
    @PutMapping("miseAJour/{id}")
    public Incident miseAjourIncident(@PathVariable int id,@RequestBody Institution institution){
        Incident incident=incidentService.rechercherIncident(id);
        Set<Incident> incidentsSimilaire=incident.getIncidentsSimilaire();

        if (incident.getStatutIncident()==Statut.signalée){
            for (Incident i:
                 incidentsSimilaire) {
                i.setStatutIncident(Statut.En_cour);
               incidentService.enregistrerIncident(i);
            }
            incident.setStatutIncident(Statut.En_cour);
            incident.getInstitutionsConcerne().add(institution);
        }
        else if (incident.getStatutIncident()==Statut.En_cour){
            for (Incident i:
                    incidentsSimilaire) {
                i.setStatutIncident(Statut.Resolu);
                incidentService.enregistrerIncident(i);
            }
            incident.setStatutIncident(Statut.Resolu);
        }
        return incidentService.enregistrerIncident(incident);
    }

    @GetMapping("tauxResolutionQuartier/{nom}")
    public float tauxResolutionQuartier(@PathVariable String nom){
        float taux=0;
        float  nbreTotal=0;
        float nbreResolu=0;
        Set<Incident> incidentsQuartier=new HashSet<>();
        List<Incident> incidents=incidentService.listeIncident();
        for (Incident i:
             incidents) {
            if (i.getLocalisation().getQuartier().equals(nom)){
                incidentsQuartier.add(i);
            }
        }
        nbreTotal=incidentsQuartier.size();

        for (Incident i:
             incidentsQuartier) {
            if (i.getStatutIncident().equals(Statut.Resolu)){
                nbreResolu++;
            }
        }
        taux=(nbreResolu/nbreTotal)*100;
        System.out.println(nbreResolu);
        System.out.println(nbreTotal);
        taux= (float) (Math.round(taux*100.0)/100.0);
        return taux;


    }

    @GetMapping("listeQuartierInci")
    public Set<String> listeQuartierIncident(){
        Set<String> quartiers=new HashSet<>();
        List<Incident> incidents=incidentService.listeIncident();
        for (Incident i:incidents) {
            quartiers.add(i.getLocalisation().getQuartier());
        }
        return quartiers;
    }


}
