package com.isj.Incidenz.presentation.apicontroller;


import com.isj.Incidenz.model.Admin;
import com.isj.Incidenz.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/admin/")
@CrossOrigin
public class AdminController {

    @Autowired
    private AdminService adminService;

    @PostMapping("enregistrer")
    public Admin enregistrerAdmmin(@RequestBody Admin admin){
        return adminService.enregistrerAdmin(admin);
    }
    @GetMapping("connexion/{email}/{mdp}")
    public Admin connexion(@PathVariable String email, @PathVariable String mdp){
        return adminService.rechercherEmailMpd(email,mdp);
    }

}
