package com.isj.Incidenz.presentation.apicontroller;


import com.google.api.client.json.Json;
import com.google.api.client.json.JsonString;
import com.google.gson.JsonObject;
import com.google.protobuf.StringValue;
import com.google.protobuf.util.JsonFormat;
import com.isj.Incidenz.model.Admin;
import com.isj.Incidenz.model.Institution;
import com.isj.Incidenz.service.AdminService;
import com.isj.Incidenz.service.InstitutionService;
import javafx.util.StringConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;

@RestController
@CrossOrigin
@RequestMapping("api/auth/")
public class authController {

    @Autowired
    private InstitutionService institutionService;

    @Autowired
    private AdminService adminService;

    @GetMapping(value = "connexion/{email}/{mdp}")
    public String connexion(@PathVariable String email, @PathVariable String mdp){
        Admin admin=null;
        Institution institution=null;

        institution = institutionService.rechercherEmailMpd(email,mdp);
        admin=adminService.rechercherEmailMpd(email, mdp);

        if (institution != null ){
            return institution.toString();
        }
        else if (admin != null){
            return admin.toString();
        }
        else {
            return "null";
        }

    }
}
