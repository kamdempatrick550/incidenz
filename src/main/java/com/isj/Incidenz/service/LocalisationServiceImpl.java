package com.isj.Incidenz.service;

import com.isj.Incidenz.model.Localisation;
import com.isj.Incidenz.repository.LocalisationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocalisationServiceImpl implements LocalisationService {

    @Autowired
    private LocalisationRepo localisationRepo;

    @Override
    public List<Localisation> listeLocalisation() {
        return localisationRepo.findAll();
    }

    @Override
    public Localisation rechercherLocalisation(int id) {
        return localisationRepo.findById(id).get();
    }

    @Override
    public Localisation enregistrerLocalisation(Localisation localisation) {
        return localisationRepo.save(localisation);
    }

    @Override
    public void supprimerLocalisation(int id) {
        localisationRepo.deleteById(id);
    }

    @Override
    public double distance(double lon1, double lat1,double lon2,double lat2) {//Recupere les latitudes et longitudes et renvoit la distance
        lon1=Math.toRadians(lon1);
        lon2=Math.toRadians(lon2);
        lat1=Math.toRadians(lat1);
        lat2=Math.toRadians(lat2);


        double d= 6371 * Math.acos( Math.sin(lat1)*Math.sin(lat2) +  Math.cos(lat1)*Math.cos(lat2)*Math.cos(lon2-lon1) );
        System.out.println(d);
        return d;
    }
}
