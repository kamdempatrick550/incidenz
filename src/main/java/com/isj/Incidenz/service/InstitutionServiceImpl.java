package com.isj.Incidenz.service;

import com.isj.Incidenz.model.Institution;
import com.isj.Incidenz.model.TypeInstituton;
import com.isj.Incidenz.repository.InstitutionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class InstitutionServiceImpl implements InstitutionService {

    @Autowired
    private InstitutionRepo institutionRepo;

    @Override
    public List<Institution> listeInstitution() {
        return institutionRepo.findAll();
    }

    @Override
    public Institution rechercherInstitution(int id) {
        return institutionRepo.findById(id).get();
    }

    @Override
    public Institution enregistrerInstitution(Institution institution) {
        return institutionRepo.save(institution);
    }

    @Override
    public void supprimerInstitution(int id) {
        institutionRepo.deleteById(id);
    }

    @Override
    public Institution rechercherEmailMpd(String email, String mdp) {
        return institutionRepo.findByEmailAndAndMdp(email,mdp);
    }

    @Override
    public Institution rechercherParNom(String nom) {
        return institutionRepo.findByNomInstitution(nom);
    }

    @Override
    public Set<Institution> rechercherParType(TypeInstituton type) {
        return institutionRepo.findAllByTypeInstituton(type);
    }
}
