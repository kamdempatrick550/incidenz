package com.isj.Incidenz.service;

import com.isj.Incidenz.model.Institution;
import com.isj.Incidenz.model.TypeInstituton;
import com.isj.Incidenz.model.Utilisateur;

import java.util.List;
import java.util.Set;

public interface InstitutionService {
    List<Institution> listeInstitution();
    Institution rechercherInstitution(int id);
    Institution enregistrerInstitution(Institution institution);
    void supprimerInstitution(int id);

    Institution rechercherEmailMpd(String email,String mdp);
    Institution rechercherParNom(String nom);
    Set<Institution> rechercherParType(TypeInstituton type);

}
