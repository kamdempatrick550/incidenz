package com.isj.Incidenz.service;

import com.isj.Incidenz.model.Institution;
import com.isj.Incidenz.model.Localisation;

import java.util.List;
import java.util.Locale;

public interface LocalisationService {

    List<Localisation> listeLocalisation();
    Localisation rechercherLocalisation(int id);
    Localisation enregistrerLocalisation(Localisation localisation);
    void supprimerLocalisation(int id);

    double distance(double lon1,double lat1,double lon2,double lat2);
}
