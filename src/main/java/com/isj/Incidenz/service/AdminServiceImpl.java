package com.isj.Incidenz.service;

import com.isj.Incidenz.model.Admin;
import com.isj.Incidenz.repository.AdminRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminRepo adminRepo;

    @Override
    public Admin enregistrerAdmin(Admin admin) {
        return adminRepo.save(admin);
    }

    @Override
    public Admin rechercherEmailMpd(String email, String mdp) {
        return adminRepo.findByEmailAndAndMdp(email,mdp);
    }
}
