package com.isj.Incidenz.service;

import com.isj.Incidenz.model.Utilisateur;

import java.util.List;

public interface IUtilisateurService {
    List<Utilisateur> listeUtilisateur();
    Utilisateur rechercherUtilisateur(int id);
    Utilisateur enregistrerUtilisateur(Utilisateur utilisateur);
    void supprimerUtilisateur(int id);
}
