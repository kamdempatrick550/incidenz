package com.isj.Incidenz.service;

import com.isj.Incidenz.model.Admin;

public interface AdminService {

    Admin enregistrerAdmin(Admin admin);

    Admin rechercherEmailMpd(String email, String mdp);
}
