package com.isj.Incidenz.service;

import com.isj.Incidenz.model.Utilisateur;
import com.isj.Incidenz.repository.UtilisateurRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UtilisateurServiceImpl implements IUtilisateurService{

    @Autowired
    private UtilisateurRepo utilisateurRepo;

    @Override
    public List<Utilisateur> listeUtilisateur() {
        return utilisateurRepo.findAll();
    }

    @Override
    public Utilisateur rechercherUtilisateur(int id) {
        return utilisateurRepo.findById(id).get();
    }

    @Override
    public Utilisateur enregistrerUtilisateur(Utilisateur utilisateur) {
        return utilisateurRepo.save(utilisateur);
    }

    @Override
    public void supprimerUtilisateur(int id) {
        utilisateurRepo.deleteById(id);
    }
}
