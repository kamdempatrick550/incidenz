package com.isj.Incidenz.service;

import com.isj.Incidenz.model.Incident;
import com.isj.Incidenz.model.Institution;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IncidentService {

    List<Incident> listeIncident();
    Incident rechercherIncident(int id);
    Incident enregistrerIncident(Incident incident);
    void supprimerIncident(int id);
    Page<Incident> incdentRecents(Pageable pageable);
}
