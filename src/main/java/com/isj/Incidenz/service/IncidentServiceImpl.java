package com.isj.Incidenz.service;

import com.isj.Incidenz.model.Incident;
import com.isj.Incidenz.repository.IncidentRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IncidentServiceImpl implements IncidentService {

    @Autowired
    private IncidentRepo incidentRepo;

    @Override
    public List<Incident> listeIncident() {
        return incidentRepo.findAll();
    }

    @Override
    public Incident rechercherIncident(int id) {
        return incidentRepo.findById(id).get();
    }

    @Override
    public Incident enregistrerIncident(Incident incident) {
        return incidentRepo.save(incident);
    }

    @Override
    public void supprimerIncident(int id) {
        incidentRepo.deleteById(id);
    }

    @Override
    public Page<Incident> incdentRecents(Pageable pageable) {
        return  incidentRepo.findAll(pageable);
    }
}
