package com.isj.Incidenz.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Incident {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idIncident;

    private String lieu;

    @Enumerated(EnumType.STRING)
    private TypeIncident typeIncident;

    @Enumerated(EnumType.STRING)
    private Statut statutIncident;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn( name="Localisation",referencedColumnName = "idLocalisation", nullable=false )
    private Localisation localisation;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "Incident_Institution",
            joinColumns = @JoinColumn(name = "idIncident"),
            inverseJoinColumns = @JoinColumn(name = "idInstitution")
    )
    @JsonIgnore
    private List<Institution> institutionsConcerne=new ArrayList<>();

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "Incident_User",
            joinColumns = @JoinColumn(name = "idIncident"),
            inverseJoinColumns = @JoinColumn(name = "idUser")
    )
   // @JoinColumn(name = "idUser", referencedColumnName = "idUser")
    private Utilisateur user_src;



    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;

    private String description;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "Incident_IncidentSim",
            joinColumns = @JoinColumn(name = "idIncident"),
            inverseJoinColumns = @JoinColumn(name = "idIncidentSim",referencedColumnName = "idIncident")
    )
    @JsonIgnore
    private Set<Incident> incidentsSimilaire;




}
