package com.isj.Incidenz.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Utilisateur {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idUser;

    private String nom;

    @OneToOne
    @JoinColumn( name="Adresse",referencedColumnName = "idLocalisation")
    private Localisation adresse;

    private String tel;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "Incident_User",
            joinColumns = @JoinColumn(name = "idUser",referencedColumnName = "idUser"),
            inverseJoinColumns = @JoinColumn(name = "idIncident",referencedColumnName = "idIncident")
    )
    @JsonIgnore
    private List<Incident> incident_declare=new ArrayList<>();

    @Enumerated(EnumType.STRING)
    private Role role;
}
