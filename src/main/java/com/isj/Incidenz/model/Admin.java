package com.isj.Incidenz.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.api.client.json.Json;
import com.google.api.client.json.JsonString;
import com.google.protobuf.util.JsonFormat;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Admin {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idAdmin;

    private String email;

    private String mdp;

    @Enumerated(EnumType.STRING)
    private Role role;



}
