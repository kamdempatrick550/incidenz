package com.isj.Incidenz.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Localisation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idLocalisation;

    private String ville;
    private String quartier;
    private String pays;
    private String longitude;
    private String latitude;
}
