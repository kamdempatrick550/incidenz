package com.isj.Incidenz.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Institution {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idInstitution;

    private String nomInstitution;

    private String telInstitution;

    @Enumerated(EnumType.STRING)
    private TypeInstituton typeInstituton;

    private String email;

    private String mdp;


    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn( name="idLocalisation")
    private Localisation qg;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "Incident_Institution",
            joinColumns = @JoinColumn(name = "idInstitution"),
            inverseJoinColumns = @JoinColumn(name = "idIncident")
    )
    @JsonIgnore
    private List<Incident> incidents=new ArrayList<>();

    @Enumerated(EnumType.STRING)
    private Role role;


}
