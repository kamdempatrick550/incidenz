package com.isj.Incidenz.repository;

import com.isj.Incidenz.model.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UtilisateurRepo extends JpaRepository<Utilisateur,Integer> {

}
