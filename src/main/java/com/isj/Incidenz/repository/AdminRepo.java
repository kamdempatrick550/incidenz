package com.isj.Incidenz.repository;

import com.isj.Incidenz.model.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminRepo extends JpaRepository<Admin,Integer> {
    Admin findByEmailAndAndMdp(String email,String mdp);
}
