package com.isj.Incidenz.repository;

import com.isj.Incidenz.model.Localisation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LocalisationRepo extends JpaRepository<Localisation,Integer> {
}
