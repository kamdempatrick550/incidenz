package com.isj.Incidenz.repository;

import com.isj.Incidenz.model.Institution;
import com.isj.Incidenz.model.TypeInstituton;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface InstitutionRepo extends JpaRepository<Institution,Integer> {
      Institution findByEmailAndAndMdp(String email,String mdp);
      Institution findByNomInstitution(String nom);

      Set<Institution> findAllByTypeInstituton(TypeInstituton type);


}
