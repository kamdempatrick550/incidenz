package com.isj.Incidenz.repository;

import com.isj.Incidenz.model.Incident;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IncidentRepo extends JpaRepository<Incident,Integer> {

    Page<Incident> findAll(Pageable mot_cle);

}
